Rails.application.routes.draw do
  root 'posts#index'

  resources :posts do
    delete :remove_image
  end

  post 'login', to: 'posts#login'
end
