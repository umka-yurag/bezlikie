class PostsController < ApplicationController
  def index
    @posts = case params[:post_type]
      when 'usefully'
        Post.where(post_type: 'usefully')
      when 'fine'
        Post.where(post_type: 'fine')
      else
        Post.where(post_type: 'rule')
    end

    @posts = @posts.order(:title)
  end

  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.create(params[:post])

    if params[:images]
      params[:images].each do |image|
        @post.images.create(image_url: image) unless image.blank?
      end
    end

    if @post.valid?
      redirect_to post_path(@post)
    else
      render :new
    end
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    @post.update_attributes(params[:post])

    if params[:images]
      params[:images].each do |image|
        @post.images.create(image_url: image) unless image.blank?
      end
    end

    if @post.valid?
      redirect_to post_path(@post)
    else
      render :edit
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    redirect_to posts_path
  end

  def login
    cookies[:logged] = true if params[:sign][:login] == 'krotten' && params[:sign][:password] == 'khazmodan'
  end

  def remove_image
    @post = Post.find(params[:post_id])
    @post.images.find(params[:image_id]).destroy

    @post.images.reload
  end
end
