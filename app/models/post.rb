class Post < ActiveRecord::Base
  has_many :images

  attr_accessible :title, :description, :post_type
end
